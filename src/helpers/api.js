import { useState, useEffect } from "react";

export default function useAPI(endpoint) {
  const [data, setData] = useState([]);
  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const response = await fetch(endpoint);
    const responseData = await response.json();
    const loadedCities = [];

    for (const key in responseData) {
      loadedCities.push({
        id: key,
        city: responseData[key].city,
        temp: responseData[key].temp
      });
    }
    setData(loadedCities);
  };
  return data;
}
