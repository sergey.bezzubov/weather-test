import React, { useState } from "react";

import Card from "../UI/Card";
import LoadingIndicator from "../UI/LoadingIndicator";
import "./CityForm.css";

const CityForm = React.memo(props => {
  const [enteredCity, setenteredCity] = useState("");

  const submitHandler = event => {
    event.preventDefault();
    props.fetchWeather(enteredCity);
    setenteredCity("");
  };

  return (
    <section className="city-form">
      <Card>
        <form onSubmit={submitHandler}>
          <div className="form-control">
            <label htmlFor="title">Name</label>
            <input
              type="text"
              id="title"
              value={enteredCity}
              onChange={event => {
                setenteredCity(event.target.value);
              }}
            />
          </div>
          <div className="city-form__actions">
            <button type="submit">Find City</button>
            {props.loading && <LoadingIndicator />}
          </div>
        </form>
      </Card>
    </section>
  );
});

export default CityForm;
