import React from 'react';
import './CityList.css';

const CityList = props => {
  return (
    <section className="city-list">
      <h2>Loaded Cities</h2>
      <ul>
        {props.cities.map(ig => (
          <li key={ig.id} onClick={props.onRemoveItem.bind(this, ig.id)}>
            <span>{ig.city}</span>
            <span>{ig.temp} F</span>
          </li>
        ))}
      </ul>
    </section>
  );
};

export default CityList;
