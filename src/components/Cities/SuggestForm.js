import React from "react";

import Card from "../UI/Card";
import LoadingIndicator from "../UI/LoadingIndicator";
import "./CityForm.css";

const SuggestForm = React.memo(props => {
  const submitHandler = event => {
    event.preventDefault();
    props.onAddCity({ city: props.weather.city, temp: props.weather.temp });
  };

  return (
    <section className="city-form">
      <Card>
        <form onSubmit={submitHandler}>
          <div className="form-control">
            <p>City: {props.weather.city}</p>
          </div>
          <div className="form-control">
            <p>Temp: {props.weather.temp} F</p>
          </div>
          <div className="city-form__actions">
            <button type="submit">Add City</button>
            {props.loading && <LoadingIndicator />}
          </div>
        </form>
      </Card>
    </section>
  );
});

export default SuggestForm;
