import React, { useReducer, useState, useEffect } from "react";
import { cityReducer, httpReducer } from "./reducers";
import { baseUrl, suffix, endpoint, interval } from "./constants";
import useAPI from "./helpers/api";
import useInterval from "./helpers/interval";
import SuggestForm from "./components/Cities/SuggestForm";
import CityForm from "./components/Cities/CityForm";
import CityList from "./components/Cities/CityList";
import ErrorModal from "./components/UI/ErrorModal";

const App = props => {
  const savedCities = useAPI(endpoint);
  const [userCities, dispatch] = useReducer(cityReducer, []);
  const [httpState, dispatchHttp] = useReducer(httpReducer, {
    loading: false,
    error: null
  });
  const [message, setMessage] = useState(null);
  const [weather, setWeather] = useState(null);

  useEffect(
    () => {
      dispatch({ type: "SET", cities: savedCities });
    },
    [savedCities]
  );

  useInterval(async () => {
    const updateCities = await fetchActualTemp(userCities);
    dispatch({ type: "SET", cities: updateCities });
  }, interval);

  const addCityHandler = async city => {
    dispatchHttp({ type: "SEND" });
    try {
      const response = await fetch(endpoint, {
        method: "POST",
        body: JSON.stringify(city),
        headers: { "Content-Type": "application/json" }
      });
      dispatchHttp({ type: "RESPONSE" });
      const responseData = await response.json();
      dispatch({
        type: "ADD",
        city: { id: responseData.name, ...city }
      });
    } catch (e) {
      dispatchHttp({ type: "ERROR", errorMessage: "Something went wrong" });
    }
  };

  const removeCityHandler = async cityId => {
    dispatchHttp({ type: "SEND" });
    try {
      await fetch(
        `https://testcompr-e84d7.firebaseio.com/cities/${cityId}.json`,
        { method: "DELETE" }
      );
      dispatchHttp({ type: "RESPONSE" });
      dispatch({ type: "DELETE", id: cityId });
    } catch (e) {
      dispatchHttp({ type: "ERROR", errorMessage: "Something went wrong" });
    }
  };

  const clearError = () => {
    dispatchHttp({ type: "CLEAR" });
  };

  const fetchWeather = async city => {
    try {
      const response = await fetch(baseUrl + city + suffix);
      const responseData = await response.json();
      const cityTemp = responseData.main;
      cityTemp.city = responseData.name;
      setWeather(cityTemp);
    } catch (e) {
      setWeather(null);
      setMessage({ type: "error", errorMessage: "City not found" });
      return setInterval(() => setMessage(null), 5000);
    }
  };

  return (
    <div className="App">
      {httpState.error && (
        <ErrorModal onClose={clearError}>{httpState.error}</ErrorModal>
      )}
      <CityForm fetchWeather={fetchWeather} />
      {message && (
        <div className="form-control">
          <p>{message.errorMessage}</p>
        </div>
      )}
      {weather && (
        <SuggestForm
          onAddCity={addCityHandler}
          weather={weather}
          loading={httpState.loading}
        />
      )}
      <section>
        <CityList cities={userCities} onRemoveItem={removeCityHandler} />
      </section>
    </div>
  );
};

async function fetchActualTemp(userCities) {
  return await Promise.all(
    userCities.map(async function(city) {
      const response = await fetch(baseUrl + city.city + suffix);
      const responseData = await response.json();
      city.temp = responseData.main.temp;
      return city;
    })
  );
}

export default App;
