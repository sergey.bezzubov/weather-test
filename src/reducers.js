export const cityReducer = (currentcities, action) => {
  switch (action.type) {
    case 'SET':
      return action.cities;
    case 'ADD':
      return [...currentcities, action.city];
    case 'DELETE':
      return currentcities.filter(ing => ing.id !== action.id);
    default:
      throw new Error('Should not get there!');
  }
};

export const httpReducer = (curHttpState, action) => {
  switch (action.type) {
    case 'SEND':
      return { loading: true, error: null };
    case 'RESPONSE':
      return { ...curHttpState, loading: false };
    case 'ERROR':
      return { loading: false, error: action.errorMessage };
    case 'CLEAR':
      return { ...curHttpState, error: null };
    default:
      throw new Error('Should not be reached!');
  }
};
