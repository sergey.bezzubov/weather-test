const baseUrl = "http://api.openweathermap.org/data/2.5/weather?q=";
const suffix = "&units=imperial&appid=d6c1dad3b2dd811ec34e5142a466f21b";
const endpoint = "https://testcompr-e84d7.firebaseio.com/cities.json";
const interval = 20000;

export { baseUrl, suffix, endpoint, interval };
